import tkinter as tk
from tkinter import *
from tkinter import ttk
from PIL import Image, ImageTk
from tkinter import messagebox
import sqlite3


conn = sqlite3.connect('mydatabase.db') #connect to db
cursor = conn.cursor() #cursor object to exeute SQL commands

cursor.execute('''
  CREATE TABLE concerns (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    ingredient TEXT NOT NULL,
    concern TEXT NOT NULL
  )
''')
def add_item(ingredient, concern) :
        cursor.execute("INSERT INTO concerns (ingredient, concern) VALUES(?, ?)", (ingredient, concern))
        conn.commit()


add_item("Salicylic Acid", "acne")
add_item("Benzoyl Peroxide", "acne")
add_item("Glycolic Acid", "acne")
add_item("Retinoids (Retinol, Tretinoin)", "acne")
add_item("Sulfur", "acne")
add_item("Tea Tree Oi", "acne")
add_item("Azelaic Acid", "acne")
add_item("Niacinamide (Vitamin B3)", "acne")
add_item("Hyaluronic Acid", "acne")
add_item("Clay", "acne")
add_item("Zinc", "acne")
add_item("Witch Hazel", "acne")
add_item("Aloe Vera", "acne")
add_item("Green Tea Extract", "acne")
add_item("Lactic Acid", "acne")
add_item("Mandelic Acid", "acne")

add_item("Hydroquinone", "hyperpigmentation")
add_item("Kojic Acid", "hyperpigmentation")
add_item("Vitamin C", "hyperpigmentation")
add_item("Niacinamide", "hyperpigmentation")
add_item("Azelaic Acid", "hyperpigmentation")
add_item("Alpha Arbutin", "hyperpigmentation")
add_item("Licorice Extract", "hyperpigmentation")
add_item("Retinoids", "hyperpigmentation")
add_item("Glycolic Acid", "hyperpigmentation")
add_item("Lactic Acid", "hyperpigmentation")
add_item("Mandelic Acid", "hyperpigmentation")
add_item("Tranexamic Acid", "hyperpigmentation")
add_item("Mulberry Extract", "hyperpigmentation")
add_item("Bearberry Extract", "hyperpigmentation")
add_item("Vitamin E", "hyperpigmentation")
add_item("Ferulic Acid", "hyperpigmentation")
add_item("Resveratrol", "hyperpigmentation")
add_item("Vitamin B5", "hyperpigmentation")
add_item("Papaya Extract", "hyperpigmentation")
add_item("Turmeric Extract", "hyperpigmentation")

add_item("Retinoids","uneven texture")
add_item("Glycolic Acid","uneven texture")
add_item("Lactic Acid","uneven texture")
add_item("Salicylic Acid","uneven texture")
add_item("Mandelic Acid","uneven texture")
add_item("Azelaic Acid","uneven texture")
add_item("Vitamin C","uneven texture")
add_item("Peptides","uneven texture")
add_item("Ceramides","uneven texture")
add_item("Hyaluronic Acid","uneven texture")
add_item("Niacinamide","uneven texture")
add_item("Licorice Extract","uneven texture")
add_item("Willow Bark Extract","uneven texture")
add_item("Centella Asiatica","uneven texture")
add_item("Vitimine B5","uneven texture")
add_item("Rosehip Oil","uneven texture")
add_item("Jojoba Oil","uneven texture")

add_item("Emollients and Moisturizers", "eczema")
add_item("Ceramides", "eczema")
add_item("Oatmeal", "eczema")
add_item("Jojoba Oil", "eczema")
add_item("Licorice Extract", "eczema")
add_item("Aloe Vera", "eczema")
add_item("Chamomile Extract", "eczema")
add_item("Vitamin E", "eczema")
add_item("Zinc Oxide", "eczema")
add_item("Probiotics", "eczema")

add_item("Salicylic Acid Retinoids","enlarged pores")
add_item("Niacinamide","enlarged pores")
add_item("Glycolic Acid","enlarged pores")
add_item("Lactic Acid","enlarged pores")
add_item("Azelaic Acid","enlarged pores")
add_item("Witch Hazel","enlarged pores")
add_item("Clay","enlarged pores")
add_item("Tea Tree Oil","enlarged pores")
add_item("Vitamin C","enlarged pores")
add_item("Hyaluronic Acid","enlarged pores")
add_item("Jojoba Oil","enlarged pores")
add_item("Rosehip Oil","enlarged pores")
add_item("Licorice Extract","enlarged pores")
add_item("Centella Asiatica","enlarged pores")
add_item("Green Tea Extract","enlarged pores")
add_item("Zinc","enlarged pores")
add_item("Vitamin E","enlarged pores")

add_item("Hydroquinone","melasma")
add_item("Kojic Acid","melasma")
add_item("Azelaic Acid","melasma")
add_item("Vitamin C","melasma")
add_item("Niacinamide","melasma")
add_item("Retinoids","melasma")
add_item("Glycolic Acid","melasma")
add_item("Lactic Acid","melasma")
add_item("Mandelic Acid","melasma")
add_item("Tranexamic Acid","melasma")
add_item("Alpha Arbutin","melasma")
add_item("Licorice Extract","melasma")
add_item("Mulberry Extract","melasma")
add_item("Bearberry Extract","melasma")
add_item("Vitamin E","melasma")
add_item("Ferulic Acid","melasma")
add_item("Resveratrol","melasma")
add_item("Green Tea Extract","melasma")

add_item("Vitamin C","dark circles")
add_item("Vitamin K","dark circles")
add_item("Caffeine","dark circles")
add_item("Retinoids","dark circles")
add_item("Hyaluronic Acid","dark circles")
add_item("Peptides","dark circles")
add_item("Niacinamide","dark circles")
add_item("Arnica Extract","dark circles")
add_item("Licorice Extract","dark circles")
add_item("Kojic Acid","dark circles")
add_item("Alpha Arbutin","dark circles")
add_item("Green Tea Extract","dark circles")
add_item("Chamomile Extract","dark circles")
add_item("Cucumber Extract","dark circles")
add_item("Aloe Vera","dark circles")
add_item("Almond Oil","dark circles")
add_item("Rosehip Oil","dark circles")
add_item("Jojoba Oil","dark circles")
add_item("Shea Butter","dark circles")
add_item("Coconut Oil","dark circles")
add_item("Hyaluronic Acid", "none")

cursor.execute('''
  CREATE TABLE skintypes (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    ingredient TEXT NOT NULL,
    skintype TEXT NOT NULL
  )
''')

def add_ingredient(ingredient, skintype) :
  cursor.execute("INSERT INTO skintypes (ingredient, skintype) VALUES(?, ?)", (ingredient, skintype))
  conn.commit()

add_ingredient("Alpha Arbutin", "oily")
add_ingredient("Alpha Arbutin", "combination")
add_ingredient("Alpha Arbutin", "normal")
add_ingredient("Alpha Arbutin", "dry")
add_ingredient("Alpha Arbutin", "sensitive")
add_ingredient("Aloe Vera", "sensitive")
add_ingredient("Argan Oil", "dry")
add_ingredient("Argan Oil", "aging")
add_ingredient("Azelaic Acid", "oily")
add_ingredient("Bearberry Extract", "sensitive")
add_ingredient("Benzoyl Peroxide", "oily")
add_ingredient("Ceramides", "dry")
add_ingredient("Ceramides", "aging")
add_ingredient("Centella Asiatica ", "sensitive")
add_ingredient("Chamomile", "oily")
add_ingredient("Chamomile", "normal")
add_ingredient("Chamomile", "combination")
add_ingredient("Chamomile", "dry")
add_ingredient("Chamomile", "sensitive")
add_ingredient("Clay", "oily")
add_ingredient("Ferulic Acid", "aging")
add_ingredient("Glycerin", "dry")
add_ingredient("Glycolic Acid", "oily")
add_ingredient("Glycolic Acid", "combination")
add_ingredient("Glycolic Acid", "normal")
add_ingredient("Green Tea Extract", "oily")
add_ingredient("Honey", "dry")
add_ingredient("Hyaluronic Acid", "oily")
add_ingredient("Hyaluronic Acid", "combination")
add_ingredient("Hyaluronic Acid", "normal")
add_ingredient("Hyaluronic Acid", "sensitive")
add_ingredient("Hyaluronic Acid", "dry")
add_ingredient("Hyaluronic Acid", "aging")
add_ingredient("Hydroquinone", "oily")
add_ingredient("Hydroquinone", "combination")
add_ingredient("Hydroquinone", "normal")
add_ingredient("Jojoba Oil", "dry")
add_ingredient("Kojic Acid", "oily")
add_ingredient("Lactic Acid", "dry")
add_ingredient("Lactic Acid", "sensitive")
add_ingredient("Licorice Extract", "sensitive")
add_ingredient("Mandelic Acid", "oily")
add_ingredient("Mandelic Acid", "combinaton")
add_ingredient("Mandelic Acid", "normal")
add_ingredient("Mandelic Acid", "dry")
add_ingredient("Mandelic Acid", "sensitive")
add_ingredient("Marula Oil", " dry")
add_ingredient("Marula Oil", "aging")
add_ingredient("Mulberry Extract", "sensitive")
add_ingredient("Niacinamide", "oily")
add_ingredient("Niacinamide", "combination")
add_ingredient("Niacinamide", "normal")
add_ingredient("Niacinamide", "dry")
add_ingredient("Niacinamide", "sensitive")
add_ingredient("Oatmeal", "normal")
add_ingredient("Oatmeal", "dry")
add_ingredient("Oatmeal", "sensitive")
add_ingredient("Papaya Extract", "oily")
add_ingredient("Papaya Extract", "combination")
add_ingredient("Papaya Extract", "normal")
add_ingredient("Papaya Extract", "dry")
add_ingredient("Papaya Extract", "sensitive")
add_ingredient("Peptides", "aging")
add_ingredient("ProBiotics ", "aging")
add_ingredient("ProBiotics ", "dry")
add_ingredient("ProBiotics ", "sensitive")
add_ingredient("Resveratrol", "aging")
add_ingredient("Retinoids", "oily")
add_ingredient("Retinoids", "combination")
add_ingredient("Retinoids", "normal")
add_ingredient("Rosehip Oil", "dry")
add_ingredient("Rosehip Oil", "aging")
add_ingredient("Salicylic Acid", "oily")
add_ingredient("Squalane", "dry")
add_ingredient("Squalane", "sensitive")
add_ingredient("Sulfur", "oily")
add_ingredient("Tea Tree Oil", "oily")
add_ingredient("Titanium Dioxide", "sensitive")
add_ingredient("Tranexamic Acid", "sensitive")
add_ingredient("Turmeric Extract", "sensitive")
add_ingredient("Vitamin B5", "dry")
add_ingredient("Vitamin B5", "sensitive")
add_ingredient("Vitamin C", "aging")
add_ingredient("Vitamin E", "dry")
add_ingredient("Vitamin E", "aging")
add_ingredient("Vitamin K", "oily")
add_ingredient("Vitamin K", "combination")
add_ingredient("Vitamin K", "normal")
add_ingredient("Vitamin K", "dry")
add_ingredient("Vitamin K", "sensitive")
add_ingredient("Willow Bark Extract", "oily")
add_ingredient("Willow Bark Extract", "combination")
add_ingredient("Witch Hazel", "oily")
add_ingredient("Zinc", "oily")
add_ingredient("Zinc", "combination")
add_ingredient("Zinc", "normal")
add_ingredient("Zinc", "dry")
add_ingredient("Zinc", "sensitive")
add_ingredient("Zinc Oxide", "sensitive")

cursor.execute('''
  CREATE TABLE users (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    username TEXT NOT NULL,
    email TEXT NOT NULL,
    password TEXT NOT NULL,
    mobile VARCHAR(10) NOT NULL,
    CONSTRAINT check_mobile_length CHECK (length(mobile) = 10)
  )
''')

def add_user(username, email, password, mobile):
    cursor.execute("INSERT INTO users (username, email, password, mobile) VALUES (?, ?, ?, ?)", (username, email, password, mobile))
    conn.commit()

class SignUp():
    
    def authenticate_user(self) :
        username = self.user.entry1.get()
        password = self.password.entry2.get()
        cursor.execute("SELECT * FROM users WHERE username = ? AND password = ?", (username, password))
        userdata = cursor.fetchone()
        if userdata :
            self.landingpage()
        else : 
            messagebox.showerror("Error", "Invalid username or password")

    def landingpage(self) :
        self.user.destroy()
        self.user.entry1.destroy()
        self.password.destroy()
        self.password.entry2.destroy()
        self.button.destroy()
        self.rbutton.destroy()

        landing_page = Landing_page(tkob)

    def registerpage(self) :
        self.user.destroy()
        self.user.entry1.destroy()
        self.password.destroy()
        self.password.entry2.destroy()
        self.button.destroy()
        self.rbutton.destroy()

        register_page = Register(tkob)

    def display_password(self, event) :
        self.password.entry2.config(show = ".") 

    def _init_(self,tkob):
        self.tkob = tkob
        self.login_img = Image.open('image33.png')
        self.login_img = self.login_img.resize((950, 650))
        self.login_img = ImageTk.PhotoImage(self.login_img)

        self.l_img = Label(tkob, image=self.login_img)
        self.l_img.place(x=0, y=0)


        self.user = Label(tkob, text = "Username" , font = ("Ariel", 15, "bold"))
        self.user.place(x =250, y =250)
        self.user.entry1 = Entry(tkob, font = ("Ariel", 15, "bold"))
        self.user.entry1.place(x = 400, y = 250)

        self.password = Label(tkob, text = "Password", font = ("Ariel", 15, "bold"))
        self.password.place(x = 250, y = 300)
        self.password.entry2 = Entry(tkob, font = ("Ariel",15, "bold"))
        self.password.entry2.place(x = 400, y = 300)
        self.password.entry2.bind("<KeyRelease>", self.display_password)

        self.button = Button(tkob, text = "Submit", font = ("Ariel", 15, "bold"), command = self.authenticate_user)
        self.button.place(x = 350, y = 400)

        self.rbutton = Button(tkob, text = "Register", font = ("Ariel", 15, "bold"), command = self.registerpage)
        self.rbutton.place(x = 500, y = 400)

class Register() :

    def add_user(self) :
        user_name = self.username.en1.get()
        e_mail = self.email.en2.get()
        pass_word = self.password.en3.get()
        mob_ile = self.mobile.en4.get()
        add_user(user_name, e_mail, pass_word, mob_ile)
    

    def register(self) :
        self.add_user()
        self.username.destroy()
        self.username.en1.destroy()
        self.email.destroy()
        self.email.en2.destroy()
        self.password.destroy()
        self.password.en3.destroy()
        self.mobile.destroy()
        self.mobile.en4.destroy()
        self.rbutton.destroy()

        l_page = Landing_page(tkob)

    def backPage(self) :
        self.username.destroy()
        self.username.en1.destroy()
        self.email.destroy()
        self.email.en2.destroy()
        self.password.destroy()
        self.password.en3.destroy()
        self.mobile.destroy()
        self.mobile.en4.destroy()
        self.rbutton.destroy()


        s_page = SignUp(tkob)

    def display_password(self, event) :
        self.password.en3.config(show = ".")


    def _init_(self, tkob) :
        self.tkob = tkob
        self.sign_img = Image.open('image33.png')
        self.sign_img = self.sign_img.resize((950, 650))
        self.sign_img = ImageTk.PhotoImage(self.sign_img)

        self.s_img = Label(tkob, image=self.sign_img)
        self.s_img.place(x=0, y=0)
        #self.s_img.lower()

        self.username = Label(tkob, text = "UserName", font = ("Ariel", 15, "bold"))
        self.username.place(x = 275, y = 150)
        self.username.en1 = Entry(tkob, font = ("Ariel", 15, "bold"))
        self.username.en1.place(x = 500, y = 150)

        self.email = Label(tkob, text = "Email", font = ("Ariel", 15, "bold"))
        self.email.place(x = 275, y = 200)
        self.email.en2 = Entry(tkob, font = ("Ariel", 15, "bold"))
        self.email.en2.place(x = 500, y = 200)



        self.password = Label(tkob, text = "Password", font = ("Ariel", 15, "bold"))
        self.password.place(x = 275, y = 250)
        self.password.en3 = Entry(tkob, font = ("Ariel", 15, "bold"))
        self.password.en3.place(x = 500, y = 250)
        self.password.en3.bind("<KeyRelease>", self.display_password)


        self.mobile = Label(tkob, text = "Mobile No", font = ("Ariel", 15, "bold"))
        self.mobile.place(x = 275, y = 300)
        self.mobile.en4 = Entry(tkob, font = ("Ariel", 15, "bold"))
        self.mobile.en4.place(x = 500, y = 300)

        self.rbutton = Button(tkob, text = "Create an Account", font = ("Ariel", 15, "bold"), command = self.register)
        self.rbutton.place(x = 400, y = 400)

        self.back_button = Button(tkob, text = "\u2B05", font = ("Ariel", 15, "bold"), command = self.backPage)
        self.back_button.place(x = 10, y = 10)
        



class Landing_page():
    
    def next_page(self):
        #print(" Work in Progress......")
        skintype = self.skin_type_dropdown.get()
        skinconcern = self.skin_concern_dropdown.get()

        self.name.destroy()
        self.tag.destroy()
        self.bg_img.destroy()
        self.skin_type_label.destroy()
        self.skin_type_dropdown.destroy()
        self.skin_concern_label.destroy()
        self.skin_concern_dropdown.destroy()
        self.suggest.destroy()
        self.back_button.destroy()

        sug = Suggest(tkob, skintype, skinconcern)

    def signup(self) :
        self.name.destroy()
        self.tag.destroy()
        self.bg_img.destroy()
        self.skin_type_label.destroy()
        self.skin_type_dropdown.destroy()
        self.skin_concern_label.destroy()
        self.skin_concern_dropdown.destroy()
        self.suggest.destroy()
        self.back_button.destroy()

        sign = SignUp(tkob)

    def _init_(self, tkob):
        self.tkob = tkob
        self.bgr_img = Image.open('bgr_img.png')
        self.bgr_img = self.bgr_img.resize((950, 650))
        self.bgr_img = ImageTk.PhotoImage(self.bgr_img)

        self.bg_img = Label(tkob, image=self.bgr_img)
        self.bg_img.place(x=0, y=0)

        self.name = Label(tkob, text="Glow Genius", font=('Roman', 50, 'bold'))
        self.name.place(x=300, y=10)
        
        self.tag = Label(tkob, text = " - get the best version of your skin..", font = ('Courier New' ,13))
        self.tag.place(x=550, y=100)

        self.skin_type_label = Label(tkob, text="Skin Type:", font=('Courier New', 15, 'bold'))
        self.skin_type_label.place(x=85, y=180)

        
        self.skin_type_dropdown = ttk.Combobox(tkob, font=('Courier New', 10) )
        self.skin_type_dropdown['values'] = ['normal', 'dry', 'oily','sensitive','combination', 'aging'] 
        self.skin_type_dropdown.place(x=215, y=180)
  
        self.skin_concern_label = Label(tkob, text="Skin Concern:", font=('Courier New', 15,'bold'))
        self.skin_concern_label.place(x=500, y=180)

        self.skin_concern_dropdown = ttk.Combobox(tkob, font=('Courier New', 10))
        self.skin_concern_dropdown['values'] = ['acne', 'hyperpigmentation', 'uneven texture', 'eczema', 'melasma', 'enlarged pores','dark circles', 'None']
        self.skin_concern_dropdown.place(x=665, y=180)

        self.suggest = Button(tkob,text = "Suggest" , font = ('Times New Roman',15,'bold'),command = self.next_page)
        self.suggest.place(x = 425 , y = 300)

        self.back_button = Button(tkob, text = "\u2B05", font = ("Ariel", 15, "bold"), command = self.signup)
        self.back_button.place(x = 10, y = 10)

class Suggest():
    def backpage(self) :
        self.bg2_img.destroy()
        self.label.destroy()
        self.frame1.destroy()
        self.frame2.destroy()
        self.textbox1.destroy()
        self.textbox2.destroy()
        self.back_button.destroy()

        landing = Landing_page(tkob)


    def _init_(self,tkob, skintype, skinconcern):
        self.tkob = tkob
        self.skintype = skintype
        self.skinconcern = skinconcern

        def pro_tip(skintype) :
            if skintype == "oily" :
                List = ["Use clay mask", "Be gentle and don't over exfoliate"]
            elif skintype == "combination":
                List = ["Use spot treatment", "Adjust skincare routine seasonally"]
            elif skintype == "normal" :
                List = ["Focus on hydration and sun protection", "Avoid harsh products"]
            elif skintype == "dry" :
                List = ["Be gentle and don't over exfoliate", "Focus on skin barrier", "Apply moisturizer on damp skin"]
            elif skintype == "sensitive" :
                List = ["Use minimal products", "Focus on skin barrier"]
            elif skintype == "aging" :
                List = ["Don't skip sunscreen", "Switch to healthy lifestyle"]

            return List
            

        def get_ingredient(user_skintype, user_concern) :
            cursor.execute(f"SELECT concerns.ingredient FROM concerns JOIN skintypes ON concerns.ingredient = skintypes.ingredient WHERE concern = '{user_concern}' AND skintype = '{user_skintype}' ")
            ingredients = cursor.fetchall() #fetching single result
            return [ingredient[0] for ingredient in ingredients]
    
        self.bgr2_img = Image.open('image22.png')
        self.bgr2_img = self.bgr2_img.resize((950, 650))
        self.bgr2_img = ImageTk.PhotoImage(self.bgr2_img)

        self.bg2_img = Label(tkob, image=self.bgr2_img)
        ##Keeping reference
        self.bg2_img.image = self.bgr2_img
        self.bg2_img.place(x=0, y=0)

        self.label = tk.Label(tkob, text="Here's is your personalized skin care ingredients", font=("Arial", 20), bg="white")
        self.label.pack(pady=20) 

        self.frame1 = tk.Frame(tkob, bg = "white")
        self.frame1.pack(side = tk.TOP)

        self.frame2 = tk.Frame(tkob, bg = "white")
        self.frame2.pack(side=tk.BOTTOM)

# Create a Text widget
        #self.textlabel = tk.Label(tkob, text = )
        


        self.textbox1 = tk.Text(self.frame1, height=15, width=25)
        ingredients = get_ingredient(self.skintype, self.skinconcern)
        for ingredient in ingredients :
            self.textbox1.insert(tk.END, ingredient + "\n")
        self.textbox1.pack(padx=20, pady=20)

        self.textbox2 = tk.Text(self.frame2, height=7, width=300)
        protips = pro_tip(self.skintype)
        for protip in protips :
            self.textbox2.insert(tk.END, protip + "\n")
        self.textbox2.pack(padx=20, pady=30)  

        self.back_button = Button(tkob, text = "\u2B05", font = ("Ariel", 15, "bold"), command = self.backpage)
        self.back_button.place(x = 10, y = 10)



tkob = tk.Tk()
tkob.title("Skin Care")
signup = SignUp(tkob)
tkob.geometry('950x650+250+30')
#landing_page = Landing_page(tkob)
tkob.mainloop()
